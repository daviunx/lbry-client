<?php

namespace Lbry;

use Lbry\Request\ChannelCreateRequest;
use Lbry\Request\ChannelUpdateRequest;
use Lbry\Request\GetRequest;
use Lbry\Request\PublishRequest;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\LoaderChain;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

class Client
{
    private Serializer $serializer;

    /**
     * Lbry constructor.
     *
     * @param Serializer $serializer
     */
    public function __construct(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    public static function create(): Client
    {
        $normalizer = new GetSetMethodNormalizer(new ClassMetadataFactory(new LoaderChain([])), new CamelCaseToSnakeCaseNameConverter());

        return new self(new Serializer([$normalizer]));
    }

    public function publish(PublishRequest $request)
    {
        $data = $this->formatRequest($this->serializer->normalize($request));

        return $this->send(['method' => 'publish', "params" => $data]);
    }

    /**
     * @param array $data
     * @return array
     *
     * TODO: Create custom normalizer
     */
    private function formatRequest(array $data): array
    {
        $data = array_map(
            function ($element) {
                if (is_numeric($element))
                    return (string)$element;

                return $element;
            },
            array_filter(
                $data,
                function ($value) {
                    return null !== $value;
                }
            )
        );

        return $data;
    }

    private function send(array $data)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "http://lbrynet:5279");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);

        if (curl_errno($ch)) {
            $error_msg = curl_error($ch);

            throw new \Exception($error_msg);
        }

        curl_close($ch);

        return json_decode($response);
    }

    public function createChannel(ChannelCreateRequest $request)
    {
        $data = $this->formatRequest($this->serializer->normalize($request));

        return $this->send(['method' => 'channel_create', 'params' => $data]);
    }

    public function updateChannel(ChannelUpdateRequest $request)
    {
        $data = $this->formatRequest($this->serializer->normalize($request));

        return $this->send(['method' => 'channel_update', 'params' => $data]);
    }

    public function get(GetRequest $request)
    {
        $data = $this->formatRequest($this->serializer->normalize($request));

        return $this->send(['method' => 'get', 'params' => $data]);
    }
}
