<?php

namespace Lbry\Request;

class GetRequest
{
	private string $uri;
	private ?string $fileName = null;
	private ?string $downloadDirectory = null;
	private ?int $timeout = null;
	private ?bool $saveFile = true;
	private ?string $walletId = null;

	/**
	 * @return string
	 */
	public function getUri(): string
	{
		return $this->uri;
	}

	/**
	 * @param string $uri
	 * @return GetRequest
	 */
	public function setUri(string $uri): GetRequest
	{
		$this->uri = $uri;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getFileName(): ?string
	{
		return $this->fileName;
	}

	/**
	 * @param string|null $fileName
	 * @return GetRequest
	 */
	public function setFileName(?string $fileName): GetRequest
	{
		$this->fileName = $fileName;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getDownloadDirectory(): ?string
	{
		return $this->downloadDirectory;
	}

	/**
	 * @param string|null $downloadDirectory
	 * @return GetRequest
	 */
	public function setDownloadDirectory(?string $downloadDirectory): GetRequest
	{
		$this->downloadDirectory = $downloadDirectory;
		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getTimeout(): ?int
	{
		return $this->timeout;
	}

	/**
	 * @param int|null $timeout
	 * @return GetRequest
	 */
	public function setTimeout(?int $timeout): GetRequest
	{
		$this->timeout = $timeout;
		return $this;
	}

	/**
	 * @return bool|null
	 */
	public function getSaveFile(): ?bool
	{
		return $this->saveFile;
	}

	/**
	 * @param bool|null $saveFile
	 * @return GetRequest
	 */
	public function setSaveFile(?bool $saveFile): GetRequest
	{
		$this->saveFile = $saveFile;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getWalletId(): ?string
	{
		return $this->walletId;
	}

	/**
	 * @param string|null $walletId
	 * @return GetRequest
	 */
	public function setWalletId(?string $walletId): GetRequest
	{
		$this->walletId = $walletId;
		return $this;
	}
}
