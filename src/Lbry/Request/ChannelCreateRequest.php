<?php

namespace Lbry\Request;

class ChannelCreateRequest
{
	private string $name;
	private float $bid;
	private bool $allowDuplicateName = false;
	private ?string $title = null;
	private ?string $description = null;
	private ?string $email = null;
	private ?string $websiteUrl = null;
	private array $featured = [];
	private array $tags = [];
	private array $locations = [];
	private ?string $thumbnailUrl = null;
	private ?string $coverUrl = null;
	private ?string $accountId = null;
	private ?string $walletId = null;
	private ?array $fundingAccountIds = null;
	private ?string $claimAddress = null;
	private bool $preview = false;
	private bool $blocking = false;

    /**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 * @return ChannelCreateRequest
	 */
	public function setName(string $name): ChannelCreateRequest
	{
		$this->name = $name;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getBid(): float
	{
		return $this->bid;
	}

	/**
	 * @param float $bid
	 * @return ChannelCreateRequest
	 */
	public function setBid(float $bid): ChannelCreateRequest
	{
		$this->bid = $bid;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isAllowDuplicateName(): bool
	{
		return $this->allowDuplicateName;
	}

	/**
	 * @param bool $allowDuplicateName
	 * @return ChannelCreateRequest
	 */
	public function setAllowDuplicateName(bool $allowDuplicateName): ChannelCreateRequest
	{
		$this->allowDuplicateName = $allowDuplicateName;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getTitle(): ?string
	{
		return $this->title;
	}

	/**
	 * @param string|null $title
	 * @return ChannelCreateRequest
	 */
	public function setTitle(?string $title): ChannelCreateRequest
	{
		$this->title = $title;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getDescription(): ?string
	{
		return $this->description;
	}

	/**
	 * @param string|null $description
	 * @return ChannelCreateRequest
	 */
	public function setDescription(?string $description): ChannelCreateRequest
	{
		$this->description = $description;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getEmail(): ?string
	{
		return $this->email;
	}

	/**
	 * @param string|null $email
	 * @return ChannelCreateRequest
	 */
	public function setEmail(?string $email): ChannelCreateRequest
	{
		$this->email = $email;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getWebsiteUrl(): ?string
	{
		return $this->websiteUrl;
	}

	/**
	 * @param string|null $websiteUrl
	 * @return ChannelCreateRequest
	 */
	public function setWebsiteUrl(?string $websiteUrl): ChannelCreateRequest
	{
		$this->websiteUrl = $websiteUrl;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getFeatured(): array
	{
		return $this->featured;
	}

	/**
	 * @param array $featured
	 * @return ChannelCreateRequest
	 */
	public function setFeatured(array $featured): ChannelCreateRequest
	{
		$this->featured = $featured;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getTags(): array
	{
		return $this->tags;
	}

	/**
	 * @param array $tags
	 * @return ChannelCreateRequest
	 */
	public function setTags(array $tags): ChannelCreateRequest
	{
		$this->tags = $tags;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getLocations(): array
	{
		return $this->locations;
	}

	/**
	 * @param array $locations
	 * @return ChannelCreateRequest
	 */
	public function setLocations(array $locations): ChannelCreateRequest
	{
		$this->locations = $locations;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getThumbnailUrl(): ?string
	{
		return $this->thumbnailUrl;
	}

	/**
	 * @param string|null $thumbnailUrl
	 * @return ChannelCreateRequest
	 */
	public function setThumbnailUrl(?string $thumbnailUrl): ChannelCreateRequest
	{
		$this->thumbnailUrl = $thumbnailUrl;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getCoverUrl(): ?string
	{
		return $this->coverUrl;
	}

	/**
	 * @param string|null $coverUrl
	 * @return ChannelCreateRequest
	 */
	public function setCoverUrl(?string $coverUrl): ChannelCreateRequest
	{
		$this->coverUrl = $coverUrl;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getAccountId(): ?string
	{
		return $this->accountId;
	}

	/**
	 * @param string|null $accountId
	 * @return ChannelCreateRequest
	 */
	public function setAccountId(?string $accountId): ChannelCreateRequest
	{
		$this->accountId = $accountId;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getWalletId(): ?string
	{
		return $this->walletId;
	}

	/**
	 * @param string|null $walletId
	 * @return ChannelCreateRequest
	 */
	public function setWalletId(?string $walletId): ChannelCreateRequest
	{
		$this->walletId = $walletId;
		return $this;
	}

	/**
	 * @return array|null
	 */
	public function getFundingAccountIds(): ?array
	{
		return $this->fundingAccountIds;
	}

	/**
	 * @param array|null $fundingAccountIds
	 * @return ChannelCreateRequest
	 */
	public function setFundingAccountIds(?array $fundingAccountIds): ChannelCreateRequest
	{
		$this->fundingAccountIds = $fundingAccountIds;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getClaimAddress(): ?string
	{
		return $this->claimAddress;
	}

	/**
	 * @param string|null $claimAddress
	 * @return ChannelCreateRequest
	 */
	public function setClaimAddress(?string $claimAddress): ChannelCreateRequest
	{
		$this->claimAddress = $claimAddress;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isPreview(): bool
	{
		return $this->preview;
	}

	/**
	 * @param bool $preview
	 * @return ChannelCreateRequest
	 */
	public function setPreview(bool $preview): ChannelCreateRequest
	{
		$this->preview = $preview;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isBlocking(): bool
	{
		return $this->blocking;
	}

	/**
	 * @param bool $blocking
	 * @return ChannelCreateRequest
	 */
	public function setBlocking(bool $blocking): ChannelCreateRequest
	{
		$this->blocking = $blocking;
		return $this;
	}
}
