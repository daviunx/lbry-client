<?php

namespace Lbry\Request;

class ChannelUpdateRequest
{
    private string $claimId;
    private float $bid;
    private ?string $title = null;
    private ?string $description = null;
    private ?string $email = null;
    private ?string $websiteUrl = null;
    private array $featured = [];
    private array $tags = [];
    private array $locations = [];
    private ?string $thumbnailUrl = null;
    private ?string $coverUrl = null;
    private ?string $accountId = null;
    private ?string $walletId = null;
    private ?array $fundingAccountIds = null;
    private ?string $claimAddress = null;
    private bool $preview = false;
    private bool $blocking = false;

    /**
     * @return string
     */
    public function getClaimId(): string
    {
        return $this->claimId;
    }

    /**
     * @param string $claimId
     * @return ChannelUpdateRequest
     */
    public function setClaimId(string $claimId): ChannelUpdateRequest
    {
        $this->claimId = $claimId;
        return $this;
    }

    /**
     * @return float
     */
    public function getBid(): float
    {
        return $this->bid;
    }

    /**
     * @param float $bid
     * @return ChannelUpdateRequest
     */
    public function setBid(float $bid): ChannelUpdateRequest
    {
        $this->bid = $bid;
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     * @return ChannelUpdateRequest
     */
    public function setTitle(?string $title): ChannelUpdateRequest
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return ChannelUpdateRequest
     */
    public function setDescription(?string $description): ChannelUpdateRequest
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return ChannelUpdateRequest
     */
    public function setEmail(?string $email): ChannelUpdateRequest
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getWebsiteUrl(): ?string
    {
        return $this->websiteUrl;
    }

    /**
     * @param string|null $websiteUrl
     * @return ChannelUpdateRequest
     */
    public function setWebsiteUrl(?string $websiteUrl): ChannelUpdateRequest
    {
        $this->websiteUrl = $websiteUrl;
        return $this;
    }

    /**
     * @return array
     */
    public function getFeatured(): array
    {
        return $this->featured;
    }

    /**
     * @param array $featured
     * @return ChannelUpdateRequest
     */
    public function setFeatured(array $featured): ChannelUpdateRequest
    {
        $this->featured = $featured;
        return $this;
    }

    /**
     * @return array
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @param array $tags
     * @return ChannelUpdateRequest
     */
    public function setTags(array $tags): ChannelUpdateRequest
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * @return array
     */
    public function getLocations(): array
    {
        return $this->locations;
    }

    /**
     * @param array $locations
     * @return ChannelUpdateRequest
     */
    public function setLocations(array $locations): ChannelUpdateRequest
    {
        $this->locations = $locations;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getThumbnailUrl(): ?string
    {
        return $this->thumbnailUrl;
    }

    /**
     * @param string|null $thumbnailUrl
     * @return ChannelUpdateRequest
     */
    public function setThumbnailUrl(?string $thumbnailUrl): ChannelUpdateRequest
    {
        $this->thumbnailUrl = $thumbnailUrl;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCoverUrl(): ?string
    {
        return $this->coverUrl;
    }

    /**
     * @param string|null $coverUrl
     * @return ChannelUpdateRequest
     */
    public function setCoverUrl(?string $coverUrl): ChannelUpdateRequest
    {
        $this->coverUrl = $coverUrl;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAccountId(): ?string
    {
        return $this->accountId;
    }

    /**
     * @param string|null $accountId
     * @return ChannelUpdateRequest
     */
    public function setAccountId(?string $accountId): ChannelUpdateRequest
    {
        $this->accountId = $accountId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getWalletId(): ?string
    {
        return $this->walletId;
    }

    /**
     * @param string|null $walletId
     * @return ChannelUpdateRequest
     */
    public function setWalletId(?string $walletId): ChannelUpdateRequest
    {
        $this->walletId = $walletId;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getFundingAccountIds(): ?array
    {
        return $this->fundingAccountIds;
    }

    /**
     * @param array|null $fundingAccountIds
     * @return ChannelUpdateRequest
     */
    public function setFundingAccountIds(?array $fundingAccountIds): ChannelUpdateRequest
    {
        $this->fundingAccountIds = $fundingAccountIds;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getClaimAddress(): ?string
    {
        return $this->claimAddress;
    }

    /**
     * @param string|null $claimAddress
     * @return ChannelUpdateRequest
     */
    public function setClaimAddress(?string $claimAddress): ChannelUpdateRequest
    {
        $this->claimAddress = $claimAddress;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPreview(): bool
    {
        return $this->preview;
    }

    /**
     * @param bool $preview
     * @return ChannelUpdateRequest
     */
    public function setPreview(bool $preview): ChannelUpdateRequest
    {
        $this->preview = $preview;
        return $this;
    }

    /**
     * @return bool
     */
    public function isBlocking(): bool
    {
        return $this->blocking;
    }

    /**
     * @param bool $blocking
     * @return ChannelUpdateRequest
     */
    public function setBlocking(bool $blocking): ChannelUpdateRequest
    {
        $this->blocking = $blocking;
        return $this;
    }
}
