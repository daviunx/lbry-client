<?php

namespace Lbry\Request;

class PublishRequest
{
	/**
	 * name of the content (can only consist of a-z A-Z 0-9 and -(dash))
	 */
	private string $name;

	/**
	 * amount to back the claim
	 */
	private float $bid;

	/**
	 * path to file to be associated with name.
	 */
	private string $filePath;

	/**
	 * validate that the video container and encodings match common web browser support or that optimization succeeds if specified. FFmpeg is required
	 */
	private bool $validateFile = false;

	/**
	 * transcode the video & audio if necessary to ensure common web browser support. FFmpeg is required
	 */
	private bool $optimizeFile = false;

	/**
	 * specify fee currency
	 */
	private ?string $feeCurrency = null;

	/**
	 * content download fee
	 */
	private ?float $feeAmount = null;

	/**
	 * address where to send fee payments, will use value from --claim_address if not provided
	 */
	private ?string $feeAddress = null;

	/**
	 * title of the publication
	 */
	private ?string $title = null;

	/**
	 * description of the publication
	 */
	private ?string $description = null;

	/**
	 * author of the publication. The usage for this field is not the same as for channels. The author field is used to credit an author who is not the publisher and is not represented by the channel.
	 * For example, a pdf file of 'The Odyssey' has an author of 'Homer' but may by published to a channel such as '@classics', or to no channel at all
	 */
	private ?string $author = null;

	/**
	 * add content tags
	 */
	private array $tags = [];

	/**
	 * languages used by the channel, using RFC 5646 format, eg: for English `en` for Spanish (Spain) `es-ES` for
	 * Spanish (Mexican) `es-MX` for Chinese (Simplified) `zh-Hans` for Chinese (Traditional) `zh-Hant`
	 */
	private array $languages = [];

	/**
	 * locations relevant to the stream, consisting of 2 letter `country` code and a `state`, `city` and a postal `code` along with a `latitude` and `longitude`. for JSON RPC: pass a dictionary with aforementioned attributes as keys, eg: ... "locations": [{'country': 'US', 'state': 'NH'}] ... for command line: pass a colon delimited list with values in the following order: "COUNTRY:STATE:CITY:CODE:LATITUDE:LONGITUDE" making sure to include colon for blank values, for example to provide only the city: ... --locations="::Manchester" with all values set: ... --locations="US:NH:Manchester:03101:42.990605:-71.460989" optionally, you can just pass the "LATITUDE:LONGITUDE": ... --locations="42.990605:-71.460989" finally, you can also pass JSON string of dictionary on the command line as you would via JSON RPC ... --locations="{'country': 'US', 'state': 'NH'}"
	 */
	private array $locations = [];

	/**
	 * publication license
	 */
	private ?string $license = null;

	/**
	 * thumbnail url
	 */
	private ?string $thumbnailUrl = null;

	/**
	 * original public release of content, seconds since UNIX epoch
	 */
	private ?int $releaseTime = null;

	/**
	 * image/video width, automatically calculated from media file
	 */
	private ?int $width = null;

	/**
	 * image/video height, automatically calculated from media file
	 */
	private ?int $height = null;

	/**
	 * audio/video duration in seconds, automatically calculated
	 */
	private ?int $duration = null;

	/**
	 * claim id of the publisher channel
	 */
	private ?string $channelId = null;

	/**
	 * name of publisher channel
	 */
	private ?string $channelName = null;

	/**
	 * none or more account ids for accounts to look in for channel certificates, defaults to all accounts.
	 */
	private ?string $channelAccountId = null;

	/**
	 * account to use for holding the transaction
	 */
	private ?string $accountId = null;

	/**
	 * restrict operation to specific wallet
	 */
	private ?string $walletId = null;

	/**
	 * ids of accounts to fund this transaction
	 */
	private array $fundingAccountIds = [];

	/**
	 * address where the claim is sent to, if not specified it will be determined automatically from the account
	 */
	private ?string $claimAddress = null;

	/**
	 * do not broadcast the transaction
	 */
	private bool $preview = false;

	/**
	 * wait until transaction is in mempool
	 */
	private bool $blocking = false;

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 * @return PublishRequest
	 */
	public function setName(string $name): PublishRequest
	{
		$this->name = $name;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getBid(): float
	{
		return $this->bid;
	}

	/**
	 * @param float $bid
	 * @return PublishRequest
	 */
	public function setBid(float $bid): PublishRequest
	{
		$this->bid = $bid;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getFilePath(): string
	{
		return $this->filePath;
	}

	/**
	 * @param string $filePath
	 * @return PublishRequest
	 */
	public function setFilePath(string $filePath): PublishRequest
	{
		$this->filePath = $filePath;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isValidateFile(): bool
	{
		return $this->validateFile;
	}

	/**
	 * @param bool $validateFile
	 * @return PublishRequest
	 */
	public function setValidateFile(bool $validateFile): PublishRequest
	{
		$this->validateFile = $validateFile;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isOptimizeFile(): bool
	{
		return $this->optimizeFile;
	}

	/**
	 * @param bool $optimizeFile
	 * @return PublishRequest
	 */
	public function setOptimizeFile(bool $optimizeFile): PublishRequest
	{
		$this->optimizeFile = $optimizeFile;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getFeeCurrency(): ?string
	{
		return $this->feeCurrency;
	}

	/**
	 * @param string|null $feeCurrency
	 * @return PublishRequest
	 */
	public function setFeeCurrency(?string $feeCurrency): PublishRequest
	{
		$this->feeCurrency = $feeCurrency;
		return $this;
	}

	/**
	 * @return float|null
	 */
	public function getFeeAmount(): ?float
	{
		return $this->feeAmount;
	}

	/**
	 * @param float|null $feeAmount
	 * @return PublishRequest
	 */
	public function setFeeAmount(?float $feeAmount): PublishRequest
	{
		$this->feeAmount = $feeAmount;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getFeeAddress(): ?string
	{
		return $this->feeAddress;
	}

	/**
	 * @param string|null $feeAddress
	 * @return PublishRequest
	 */
	public function setFeeAddress(?string $feeAddress): PublishRequest
	{
		$this->feeAddress = $feeAddress;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getTitle(): ?string
	{
		return $this->title;
	}

	/**
	 * @param string|null $title
	 * @return PublishRequest
	 */
	public function setTitle(?string $title): PublishRequest
	{
		$this->title = $title;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getDescription(): ?string
	{
		return $this->description;
	}

	/**
	 * @param string|null $description
	 * @return PublishRequest
	 */
	public function setDescription(?string $description): PublishRequest
	{
		$this->description = $description;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getAuthor(): ?string
	{
		return $this->author;
	}

	/**
	 * @param string|null $author
	 * @return PublishRequest
	 */
	public function setAuthor(?string $author): PublishRequest
	{
		$this->author = $author;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getTags(): array
	{
		return $this->tags;
	}

	/**
	 * @param array $tags
	 * @return PublishRequest
	 */
	public function setTags(array $tags): PublishRequest
	{
		$this->tags = $tags;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getLanguages(): array
	{
		return $this->languages;
	}

	/**
	 * @param array $languages
	 * @return PublishRequest
	 */
	public function setLanguages(array $languages): PublishRequest
	{
		$this->languages = $languages;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getLocations(): array
	{
		return $this->locations;
	}

	/**
	 * @param array $locations
	 * @return PublishRequest
	 */
	public function setLocations(array $locations): PublishRequest
	{
		$this->locations = $locations;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getLicense(): ?string
	{
		return $this->license;
	}

	/**
	 * @param string|null $license
	 * @return PublishRequest
	 */
	public function setLicense(?string $license): PublishRequest
	{
		$this->license = $license;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getThumbnailUrl(): ?string
	{
		return $this->thumbnailUrl;
	}

	/**
	 * @param string|null $thumbnailUrl
	 * @return PublishRequest
	 */
	public function setThumbnailUrl(?string $thumbnailUrl): PublishRequest
	{
		$this->thumbnailUrl = $thumbnailUrl;
		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getReleaseTime(): ?int
	{
		return $this->releaseTime;
	}

	/**
	 * @param int|null $releaseTime
	 * @return PublishRequest
	 */
	public function setReleaseTime(?int $releaseTime): PublishRequest
	{
		$this->releaseTime = $releaseTime;
		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getWidth(): ?int
	{
		return $this->width;
	}

	/**
	 * @param int|null $width
	 * @return PublishRequest
	 */
	public function setWidth(?int $width): PublishRequest
	{
		$this->width = $width;
		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getHeight(): ?int
	{
		return $this->height;
	}

	/**
	 * @param int|null $height
	 * @return PublishRequest
	 */
	public function setHeight(?int $height): PublishRequest
	{
		$this->height = $height;
		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getDuration(): ?int
	{
		return $this->duration;
	}

	/**
	 * @param int|null $duration
	 * @return PublishRequest
	 */
	public function setDuration(?int $duration): PublishRequest
	{
		$this->duration = $duration;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getChannelId(): ?string
	{
		return $this->channelId;
	}

	/**
	 * @param string|null $channelId
	 * @return PublishRequest
	 */
	public function setChannelId(?string $channelId): PublishRequest
	{
		$this->channelId = $channelId;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getChannelName(): ?string
	{
		return $this->channelName;
	}

	/**
	 * @param string|null $channelName
	 * @return PublishRequest
	 */
	public function setChannelName(?string $channelName): PublishRequest
	{
		$this->channelName = $channelName;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getChannelAccountId(): ?string
	{
		return $this->channelAccountId;
	}

	/**
	 * @param string|null $channelAccountId
	 * @return PublishRequest
	 */
	public function setChannelAccountId(?string $channelAccountId): PublishRequest
	{
		$this->channelAccountId = $channelAccountId;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getAccountId(): ?string
	{
		return $this->accountId;
	}

	/**
	 * @param string|null $accountId
	 * @return PublishRequest
	 */
	public function setAccountId(?string $accountId): PublishRequest
	{
		$this->accountId = $accountId;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getWalletId(): ?string
	{
		return $this->walletId;
	}

	/**
	 * @param string|null $walletId
	 * @return PublishRequest
	 */
	public function setWalletId(?string $walletId): PublishRequest
	{
		$this->walletId = $walletId;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getFundingAccountIds(): array
	{
		return $this->fundingAccountIds;
	}

	/**
	 * @param array $fundingAccountIds
	 * @return PublishRequest
	 */
	public function setFundingAccountIds(array $fundingAccountIds): PublishRequest
	{
		$this->fundingAccountIds = $fundingAccountIds;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getClaimAddress(): ?string
	{
		return $this->claimAddress;
	}

	/**
	 * @param string|null $claimAddress
	 * @return PublishRequest
	 */
	public function setClaimAddress(?string $claimAddress): PublishRequest
	{
		$this->claimAddress = $claimAddress;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isPreview(): bool
	{
		return $this->preview;
	}

	/**
	 * @param bool $preview
	 * @return PublishRequest
	 */
	public function setPreview(bool $preview): PublishRequest
	{
		$this->preview = $preview;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isBlocking(): bool
	{
		return $this->blocking;
	}

	/**
	 * @param bool $blocking
	 * @return PublishRequest
	 */
	public function setBlocking(bool $blocking): PublishRequest
	{
		$this->blocking = $blocking;
		return $this;
	}
}